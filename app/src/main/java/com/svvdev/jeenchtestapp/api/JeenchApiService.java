package com.svvdev.jeenchtestapp.api;


import com.svvdev.jeenchtestapp.entity.Response;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface JeenchApiService {

    @GET("search-items")
    Observable<Response> getResponse();

}
