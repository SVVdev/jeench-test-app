package com.svvdev.jeenchtestapp;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class ItemViewHolder extends RecyclerView.ViewHolder {
    TextView itemName, itemPrice, pointDistance;
    ImageView itemImage, shopLogo;
    ProgressBar shopRank;


    public ItemViewHolder(View itemView) {
        super(itemView);

        itemName = itemView.findViewById(R.id.itemName);
        itemPrice = itemView.findViewById(R.id.itemPrice);
        itemImage = itemView.findViewById(R.id.itemImage);
        pointDistance = itemView.findViewById(R.id.pointDistance);
        shopLogo = itemView.findViewById(R.id.shopLogo);
        shopRank = itemView.findViewById(R.id.circle_progress_bar);

    }
}
