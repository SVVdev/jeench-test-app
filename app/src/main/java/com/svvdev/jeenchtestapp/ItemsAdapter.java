package com.svvdev.jeenchtestapp;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.svvdev.jeenchtestapp.entity.Message;

import java.util.List;

public class ItemsAdapter extends RecyclerView.Adapter<ItemViewHolder> {
    Context context;
    List<Message> items;

    public ItemsAdapter(Context context, List<Message> items){
        this.context = context;
        this.items = items;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_content_main, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        // Transformations
        Transformation transRoundedCorners = new RoundedTransformationBuilder()
                .cornerRadiusDp(4)
                .oval(false)
                .build();

        Transformation transCircle = new RoundedTransformationBuilder()
                .cornerRadiusDp(10)
                .oval(true)
                .build();

        // Bind data
        Message message = items.get(position);

        holder.itemName.setText(message.getItemName());
        // TODO use currency_id to set currency
        holder.itemPrice.setText(String.format("$%s", message.getItemPrice()));
        holder.pointDistance.setText(Integer.toString(Integer.parseInt(message.getPointDistance()) / 1000) + "km");
        // TODO set company name (using shop_id somehow)
        holder.shopRank.setProgress((int)(Double.parseDouble(message.getShopRank())/10));
        Picasso.with(context)
                .load(message.getItemImage())
                .fit()
                .transform(transRoundedCorners)
                .into(holder.itemImage);
        Picasso.with(context)
                .load(message.getShopLogo())
                .fit()
                .transform(transCircle)
                .into(holder.shopLogo);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
