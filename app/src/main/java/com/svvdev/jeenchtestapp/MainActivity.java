package com.svvdev.jeenchtestapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.svvdev.jeenchtestapp.api.JeenchApiService;
import com.svvdev.jeenchtestapp.entity.Message;
import com.svvdev.jeenchtestapp.entity.Response;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    List<Message> itemList = new ArrayList<>();
    ItemsAdapter itemsAdapter;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // RecyclerView
        recyclerView = findViewById(R.id.itemsRecyclerView);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                OrientationHelper.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        // Adapter
        itemsAdapter = new ItemsAdapter(this,itemList);

        recyclerView.setAdapter(itemsAdapter);


        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://api.jeench.com/v1/")
                .build();

        JeenchApiService jeenchApiService = retrofit.create(JeenchApiService.class);
        Observable<Response> items = jeenchApiService.getResponse();


        items.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(changedItems ->{
                   itemList.clear();
                   itemList.addAll(changedItems.getMessage());
                   itemsAdapter.notifyDataSetChanged();
                },throwable -> {
                    System.out.println("ERROR " + throwable.getMessage());
                });

    }

}
